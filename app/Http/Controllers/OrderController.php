<?php

namespace App\Http\Controllers;

use App\Buyer;
use App\Http\Resources\OrderResource;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function createOrder(Request $request)
    {
        $data = $request->input();
        $date = date("Y-m-d H:i:s");

        if(empty($data)) {
            return response()->json(["response" => "Request data is empty!"], 400);
        }

        if(!Buyer::find($data['buyerId'])) {
            return response()->json(["response" => "Buyer not found!"], 404);
        }

        $orderItems = [];

        $orderData = [
            "buyer_id" => $data["buyerId"],
            "date" => $date
        ];
        $order = Order::create($orderData);

        foreach($data["orderItems"] as $item) {
            if(count($item) === 0) {
                return response()->json(['response' => "Invalid value for OrderItem"], 400);
            }

            $orderItems[] = [
                "order_id" => $order->id,
                "product_id" => $item["productId"],
                "quantity" => $item["productQty"],
                "discount" => $item["productDiscount"],
            ];
        }

        $order->orderItems()->createMany($orderItems);
        $order->save();

        $data["orderId"] = $order->id;

        return response()->json($data, 200);
    }

    public function updateOrder(Request $request)
    {
        $data = $request->input();
        $order = Order::find($data["orderId"]);

        if(!$order) {
            return response()->json(["reponse" => "Order wasn't found!"], 404);
        }

        if(empty($data["orderItems"])) {

            return response()->json(["reponse" => "OrderItems can't be empty!"], 400);
        }

        $orderItems = [];
        foreach($data["orderItems"] as $item) {
            $orderItems[] = [
                "order_id" => $order->id,
                "product_id" => $item["productId"],
                "quantity" => $item["productQty"],
                "discount" => $item["productDiscount"],
            ];
        }

        $order->orderItems()->delete();
        $order->orderItems()->createMany($orderItems);
        $order->save();

        return response()->json(new OrderResource($order), 200);
    }

    public function getOrderById(string $id)
    {
        $order = Order::find((int)$id);

        if(!$order) {
            return response()->json(["reponse" => "Order wasn't found!"], 404);
        }

        return response()->json(["data" => new OrderResource($order)], 200);
    }

}
