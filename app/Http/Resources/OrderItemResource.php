<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     * productSum - price in cents convert in dollars - price / 100,
     * discount in percents, Example: discount 13% -> price = price * (100 - 13) / 100 = price * 0.87
     */
    public function toArray($request)
    {
        return [
            'productName' => $this->product->name,
            'productQty' => $this->quantity,
            'productPrice' => $this->product->price / 100,
            'productDiscount' => $this->discount,
            'productSum' => round(($this->product->price / 100) * $this->discount / 100 * $this->quantity, 2)
        ];
    }
}
