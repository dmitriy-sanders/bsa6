<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'orderId' => $this->id,
            'orderDate' => $this->date,
            'orderSum' => round($this->orderItems
                ->reduce(function ($sum, $item) {
                    return $sum + $item->product->price / 100 * $item->quantity * $item->discount / 100;
                }),
                2),
            'orderItems' => $this->orderItems->map(function($item) {
                return new OrderItemResource($item);
            }),
            'buyer' => [
                'buyerFullName' => $this->buyer->name . ' ' . $this->buyer->surname,
                'buyerAddress' => $this->buyer->country . ', ' . $this->buyer->city . ', ' . $this->buyer->address_line,
                'buyerPhone' => $this->buyer->phone
            ]
        ];
    }
}
