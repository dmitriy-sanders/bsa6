<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'order_id' => \App\Order::query()->inRandomOrder()->first()->id,
        'product_id' => \App\Product::query()->inRandomOrder()->first()->id,
        'quantity' => $faker->numberBetween(1, 20),
        'discount' => $faker->numberBetween(1, 100),
    ];
});
