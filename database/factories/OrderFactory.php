<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'date' => date('Y-m-d H:i:s'),
        'buyer_id' => \App\Buyer::query()->inRandomOrder()->first()->id,
    ];
});
