<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Buyer::class, 25)->create()
            ->each(function ($buyer) {
            $buyer->orders()->saveMany(
                factory(\App\Order::class, 5)->create()
                    ->each(function ($order) {
                    $order->orderItems()->saveMany(
                        factory(\App\OrderItem::class, mt_rand(2, 6))->make()
                    );
                })
            );
        });
    }
}
